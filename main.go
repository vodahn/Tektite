package main

import (
    "fmt"
    "log"
    "time"
    "sync"
    "os"

    "github.com/blevesearch/bleve/v2"
)

func Err_check(err error) {
    if err != nil {
        log.Fatal(err)
    }
}

func Initiate_crawl() {
    now := time.Now().UTC().Format(time.DateTime)

    var wg sync.WaitGroup
    
    seed_URLs := Load_seeds()
    url_chan := make(chan string, len(seed_URLs))
      
    for w := 0; w < 3; w++ {
        wg.Add(1)
        go func() {defer wg.Done(); Crawler(url_chan, now)}()
    }
    for _, seed := range seed_URLs {url_chan <- seed}
    close(url_chan)
    wg.Wait()

    fmt.Println("Crawling complete.")
}

func db_create(paths ...string) {
    for _, path := range paths {
        if _, err := os.Stat(path); err != nil {
            file, err := os.Create(path)
            Err_check(err)
            file.Close()
    }}
}

func main() {
    var err error
  
    db_create("./seed_crawl.db", "./external_crawl.db")

    opt := Option{
        Index: "index.bleve",
        Opt:   "search-hmm",
        Trim:  "trim",
    }

    Index, err = bleve.Open("index.bleve")
    if err == bleve.ErrorIndexPathDoesNotExist {
        Index, err = BuildNewIndex(opt)
        Err_check(err)
    } else {Err_check(err)}

    Initiate_crawl()
    Listen()
}