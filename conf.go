package main

import (
    "os"
    "bufio"
    //"net/url"
)

var Seed_URLs []string

func Load_seeds() []string {
    file, err := os.Open("seeds.txt")
    Err_check(err)
    defer file.Close()

    var lines []string
    scanner := bufio.NewScanner(file)
    for scanner.Scan() {
        lines = append(lines, scanner.Text())
    }

    return lines
}