package main

import (
    "time"
    "net/http"
)

func Listen() {
    mux := http.NewServeMux()
    mux.Handle("/", http.FileServer(http.Dir("./frontend")))
    mux.HandleFunc("/search/", Search)

    srv := &http.Server {
        Addr: ":1024",
        IdleTimeout: 10 * time.Second,
        ReadTimeout: 6 * time.Second,
        ReadHeaderTimeout: 6 * time.Second,
        WriteTimeout: 10 * time.Second,
        Handler: mux,
    }
    srv.ListenAndServe()
}