package main

import (
//"fmt"
    "net/http"

    "github.com/blevesearch/bleve/v2"
)

func Search(w http.ResponseWriter, req *http.Request) {
    query_string := req.FormValue("query")

    query := bleve.NewMatchQuery(query_string)
    query.FieldVal = "Body"

    searchRequest := bleve.NewSearchRequest(query)
    searchRequest.Fields = []string{"Mime", "Title"}
    searchResult, err := Index.Search(searchRequest)
    Err_check(err)

    var hits string

    for _, hit := range searchResult.Hits {
        if title := hit.Fields["Title"]; title != nil {
            hits += `<label>` + title.(string) + `</label><br>`
        }
        hits += `<a href="` + hit.ID + `">` + hit.ID + `</a><br><br>`
    }

    if len(hits) == 0 {hits += "No results."}

    w.Header().Set("Content-Type", "text/html; charset=utf-8")
    w.WriteHeader(http.StatusOK)
    w.Write([]byte(hits))
}